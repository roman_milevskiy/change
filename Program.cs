﻿using System;

namespace change
{
    class Program
    {
        static void Main(string[] args)
        {
            var changeble1 = 5;
            var changeble2 = 7;
            Console.WriteLine("Сначала: \nПеременная 1 =" + changeble1 + "\nПеременная 2 =" + changeble2);
            changeble1 = changeble1 + changeble2; // =12
            changeble2 = changeble2 - changeble1; // =-5
            changeble2 = -changeble2;
            changeble1 = changeble1 - changeble2;
            Console.WriteLine("После замены: \nПеременная 1 =" + changeble1 + "\nПеременная 2 =" + changeble2 + ".");
            Console.ReadKey();
        }
    }
}
